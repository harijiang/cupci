Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    omniauth_callbacks: "users/omniauth_callbacks",
    passwords: "users/passwords",
    confirmations: "users/confirmations",
    unlocks: "users/unlocks"
  }

  # mostly resource under space namespace
  resources :spaces, only: [:new, :create, :index] do
    resources :pipelines, only: [:index, :new, :create, :edit, :update, :show, :destroy] do
      member do
        get 'configuration'
      end
      resources :actions, only: [:new, :create], module: :pipelines
    end

    resources :actions, only: [:index, :show] do
      member do
        post 'rebuild'
      end
    end
    resources :templates, only: [:index]
  end

  get '/repos/github_repos', to: 'repos#github_repos', as: 'github_repos'

  get 'dashboard', to: 'dashboard#index'
  root 'dashboard#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
