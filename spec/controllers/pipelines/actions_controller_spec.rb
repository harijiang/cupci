require 'rails_helper'

RSpec.describe Pipelines::ActionsController, type: :controller do

  let(:space) {create :space}
  let(:user) {create(:user).tap {|u| u.spaces << space}}
  let(:pipeline) {
    create :pipeline, space: space, user: user
  }

  it 'Create action' do
    space
    user
    repo = create :repo
    template = create :template
    pipeline = create :pipeline, space: space, user: user, source: repo, template: template
    sign_in user
    post :create,
         params: {
           pipeline_id: pipeline.id,
           space_id: space.id
         },
         format: :js
    action = assigns(:action)
    expect(action.triggerable).to eq user
    expect(action.space).to eq space
    expect(action.pipeline).to eq pipeline
    expect(action.sequence).to eq 1
  end

  it 'Create action trigger backend worker' do
    sign_in user
    expect {
      post :create,
           params: {
             pipeline_id: pipeline.id,
             space_id: space.id
           },
           format: :js
    }.to change(PipelineWorker.jobs, :size).by(1)
  end

end
