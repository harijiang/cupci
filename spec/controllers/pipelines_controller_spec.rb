require 'rails_helper'

RSpec.describe PipelinesController, type: :controller do
  let(:template) {create(:template, template_status: :published)}
  let(:auth) {create :auth}
  let(:space) {create :space}
  let(:user) {create(:user).tap {|u|
    u.auths << auth
    u.spaces << space
  }}
  let (:pipeline) {create(:pipeline, space: space, user: user, template: template)}

  it 'create pipelines' do
    sign_in user
    post :create,
         params: {
           pipeline: {
             name: 'test',
             template_id: template.id,
             source: {reponame: 'cupci/example', auth_id: auth.id},
             project: {name: 'example'},
             configuration: {docker_image: 'centos'}
           },
           space_id: space.id,
         }
    pipeline = assigns(:pipeline)
    expect(pipeline.persisted?).to be true
    expect(pipeline.user).to eq user
    expect(pipeline.space).to eq space
    expect(pipeline.source.reponame).to eq 'cupci/example'
    expect(pipeline.source.auth.attributes).to eq auth.attributes # ignore type casting
    expect(pipeline.template).to eq template
    expect(pipeline.name).to eq 'test'
    expect(pipeline.project.name).to eq 'example'
    expect(pipeline.configuration).to eq({"docker_image" => "centos"})
  end

  it 'pipeline share projects, but not share source repo' do
    sign_in user
    post :create,
         params: {
           pipeline: {
             name: 'test',
             template_id: template.id,
             source: {reponame: 'cupci/example', auth_id: auth.id},
             project: {name: 'example'},
             configuration: {}
           },
           space_id: space.id,
         }
    pipeline1 = assigns(:pipeline)
    # clear @pipeline instances
    [:@pipeline, :@template].each {|i| controller.instance_variable_set i, nil}
    post :create,
         params: {
           pipeline: {
             name: 'test2',
             template_id: template.id,
             source: {reponame: 'cupci/example', auth_id: auth.id},
             project: {name: 'example'},
             configuration: {}
           },
           space_id: space.id,
         }
    pipeline2 = assigns(:pipeline)
    expect(pipeline1.project).to eq pipeline2.project
    expect(pipeline1.source).to_not eq pipeline2.source
  end

  it 'update pipeline' do
    sign_in user
    patch :update,
          params: {
            pipeline: {
              name: 'new_name',
              template_id: template.id,
              source: {reponame: 'cupci/new_example', auth_id: auth.id},
              project: {name: 'new_project'},
              configuration: {docker_image: 'ubuntu'}
            },
            space_id: space.id,
            id: pipeline.id,
          }

    expect(response).to redirect_to(action: :show, id: pipeline.id, space_id: space.id)
    pipeline2 = assigns(:pipeline)
    expect(pipeline2.id).to eq pipeline.id
    expect(pipeline2.project.name).to eq 'new_project'
    expect(pipeline2.project.id).to_not eq pipeline.project.id
    expect(pipeline2.configuration).to eq({'docker_image' => 'ubuntu'})
    # repo id should not change
    expect(pipeline2.source.reponame).to eq 'cupci/new_example'
    expect(pipeline2.source.id).to eq pipeline.source.id
  end

  it 'update pipeline failure' do
    sign_in user
    patch :update,
          params: {
            pipeline: {
              name: '',
              template_id: template.id,
              source: {reponame: '', auth_id: auth.id},
              project: {name: ''},
              configuration: {docker_image: 'ubuntu'}
            },
            space_id: space.id,
            id: pipeline.id,
          }

    expect(response).to render_template("edit")

    pipeline2 = assigns(:pipeline)
    expect(pipeline2.errors.present?).to be true
    expect(pipeline2.project.errors.present?).to be true
    expect(pipeline2.source.errors.present?).to be true
  end
end
