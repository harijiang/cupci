require 'rails_helper'

RSpec.describe TemplatesController, type: :controller do

  let(:space) {create :space}
  let(:user) {create(:user).tap {|u| u.spaces << space}}

  it 'Only list published template' do
    sign_in user
    create(:template)

    get :index, params: {space_id: space.id}
    templates = assigns(:templates)
    expect(templates.blank?).to be_truthy
    published_template = create(:template, template_status: :published)

    get :index, params: {space_id: space.id}
    templates = assigns(:templates)
    expect(templates.first).to eq published_template
    expect(templates.count).to eq 1
  end
end
