require 'rails_helper'

RSpec.describe SpacesController, type: :controller do
  let(:user) {create(:user)}

  it 'create space' do
    sign_in user
    post :create,
         params: {
           space: {name: 'test'}
         }, format: :js
    space = assigns(:space)
    expect(space.persisted?).to be true
    expect(space.users.first).to eq user
    expect(space.name).to eq 'test'
    expect(space.spaces_users.find_by(user: user).admin?).to be true
  end
end
