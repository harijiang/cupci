# == Schema Information
#
# Table name: templates
#
#  id              :integer          not null, primary key
#  name            :string
#  content         :jsonb
#  sequence        :integer
#  template_status :integer          default("privates")
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  space_id        :integer
#
# Indexes
#
#  index_templates_on_sequence         (sequence)
#  index_templates_on_space_id         (space_id)
#  index_templates_on_template_status  (template_status)
#
# Foreign Keys
#
#  fk_rails_...  (space_id => spaces.id)
#

require 'rails_helper'

RSpec.describe Template, type: :model do
  context 'load and parse templates' do
    before do
      Template.load_templates scope: 'test'
    end

    after do
      Template.delete_all
    end

    it 'load all test templates' do
      expect(Template.all.count).to be > 0
      expect(Template.all.all? {|t| t.parse.scope == 'test'}).to be true
    end

    it 'parse yaml' do
      expect(Template.find_by(name: 'example').stages.count).to be > 0
    end
  end

  context 'templates query scope' do

    before do
      space1 = create :space, name: 'space1'
      space2 = create :space, name: 'space2'

      # defaults
      5.times {create :template, template_status: :published}
      create :template

      # space1
      3.times {create :template, space: space1, template_status: :published}
      2.times {create :template, space: space1, template_status: :privates}

      # space2
      2.times {create :template, space: space2}
    end

    after do
      [Template, SpacesUser, User, Space].each &:delete_all
    end

    it 'default templates belongs to no space' do
      expect(Template.default_templates.count).to eq 5
      expect(Template.default_templates.all? {|t| t.space.nil?}).to be true
    end

    it 'default templates order by sequnce' do
      expect(Template.default_templates.to_a).to eq Template.default_templates.to_a.sort_by {|t| t.sequence}
    end

    it 'templates return published space templates and default templates' do
      space1 = Space.find_by(name: 'space1')
      expect(Template.templates(space1).count).to eq 8
      expect(Template.default_templates.all? {|t| t.space.nil? || (t.space == space1 && t.published?)}).to be true
    end

    it 'templates order by' do
      expect(Template.default_templates.to_a).to eq Template.default_templates.to_a.sort_by {|t| [t.space.nil? ? 0 : 1, t.sequence]}
    end
  end
end
