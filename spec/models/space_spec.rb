# == Schema Information
#
# Table name: spaces
#
#  id         :integer          not null, primary key
#  name       :string
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_spaces_on_deleted_at  (deleted_at)
#

require 'rails_helper'

RSpec.describe Space, type: :model do
  context 'Create space' do
    let (:user) {create :user}
    it "Must have owner" do
      space = user.spaces.create(name: 'hello ship')
      expect(space.errors.keys).to eq [:owner]
    end

    it "Space creator is admin" do
      space = user.spaces.create(name: 'hello ship', owner: user)
      expect(space.persisted?).to be true
      expect(space.name).to eq 'hello ship'
      expect(user.spaces_users.find_by(space: space).admin?).to be true
    end

    it 'Can not create duplicate name space' do
      space1 = user.spaces.create(name: 'hello ship', owner: user)
      space2 = user.spaces.create(name: 'hello ship', owner: user)
      expect(space1.persisted?).to be true
      expect(space2.errors.keys).to eq [:name]
    end
  end
end
