# == Schema Information
#
# Table name: repos
#
#  id         :integer          not null, primary key
#  reponame   :string
#  auth_id    :integer
#  space_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_repos_on_auth_id   (auth_id)
#  index_repos_on_space_id  (space_id)
#
# Foreign Keys
#
#  fk_rails_...  (auth_id => auths.id)
#  fk_rails_...  (space_id => spaces.id)
#

require 'rails_helper'

RSpec.describe Repo, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
