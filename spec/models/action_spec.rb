# == Schema Information
#
# Table name: actions
#
#  id               :integer          not null, primary key
#  pipeline_id      :integer
#  sequence         :integer
#  status           :integer          default("initialed")
#  current_pipeline :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  current_stage    :string
#  space_id         :integer
#  started_at       :datetime
#  finished_at      :datetime
#  triggerable_id   :integer
#  triggerable_type :string
#  configuration    :jsonb
#
# Indexes
#
#  index_actions_on_pipeline_id                          (pipeline_id)
#  index_actions_on_pipeline_id_and_sequence             (pipeline_id,sequence) UNIQUE
#  index_actions_on_sequence                             (sequence)
#  index_actions_on_space_id                             (space_id)
#  index_actions_on_status                               (status)
#  index_actions_on_triggerable_id_and_triggerable_type  (triggerable_id,triggerable_type)
#
# Foreign Keys
#
#  fk_rails_...  (pipeline_id => pipelines.id)
#  fk_rails_...  (space_id => spaces.id)
#

require 'rails_helper'

RSpec.describe Action, type: :model do
  it 'Sequence auto increase' do
    p1 = create :pipeline
    p2 = create :pipeline

    expect(create(:action, pipeline: p1).sequence).to eq 1
    expect(create(:action, pipeline: p1).sequence).to eq 2
    expect(create(:action, pipeline: p1).sequence).to eq 3

    expect(create(:action, pipeline: p2).sequence).to eq 1
    expect(create(:action, pipeline: p2).sequence).to eq 2

    expect(create(:action, pipeline: p1).sequence).to eq 4
  end

  it 'completed?' do
    action = create(:action)
    expect(action.initialed?).to be true
    expect(action.completed?).to be false
    action.successed!
    expect(action.completed?).to be true
  end
end
