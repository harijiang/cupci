require 'rails_helper'

RSpec.describe DeepStruct do

  let (:hash) {
    {a: {b: 1}, c: 2, d: {e: {f: 3}}, g: [{a: 1}, {b: {c: 2}}]}
  }

  it 'extract value' do
    value = DeepStruct.new(hash)
    expect(value.a.b).to eq 1
    expect(value.c).to eq 2
    expect(value.d.e.f).to eq 3
  end

  it 'work with array' do
    value = DeepStruct.new(hash)
    expect(value.g[0].a).to eq 1
    expect(value.g[1].b.c).to eq 2
  end

  it 'convert back to hash' do
    value = DeepStruct.new(hash)
    expect(value.a.to_h).to eq(b: 1)
    expect(value.d.e.to_h).to eq(f: 3)
  end
end
