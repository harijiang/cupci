require 'rails_helper'

RSpec.describe Executor::Manager do
  context 'Executor Manager instant executors via type' do
    it 'raise name error if type invalid' do
      expect {Executor::Manager.new_executor('unknown')}.to raise_error NameError
    end

    it 'detect class to initialize if type exists' do
      expect(Executor::Manager.new_executor('eval')).to be_a Executor::EvalExecutor
    end
  end
end
