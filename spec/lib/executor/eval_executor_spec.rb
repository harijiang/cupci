require 'rails_helper'

RSpec.describe Executor::EvalExecutor do
  it 'eval executor works fine' do
    test_context = {}
    io = StringIO.new
    executor = Executor::Manager.new_executor('eval', locals: {test_context: test_context}, stdout: io, stderr: io)
    executor.run_line "test_context[:message] = 'hello world'"
    expect(test_context).to eq({message: 'hello world'})
  end

  it 'capture stdout stderr' do
    io = StringIO.new
    executor = Executor::Manager.new_executor('eval', stdout: io, stderr: io)
    executor.run_line "stdout.puts 'hello world'"
    executor.run_line "stderr.puts 'hello Cup CI'"
    expect(io.tap(&:rewind).readlines).to eq ["hello world\n", "hello Cup CI\n"]
  end
end
