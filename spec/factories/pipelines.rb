# == Schema Information
#
# Table name: pipelines
#
#  id            :integer          not null, primary key
#  name          :string
#  space_id      :integer
#  source_type   :string
#  source_id     :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  ancestry      :string
#  template_id   :integer
#  user_id       :integer
#  project_id    :integer
#  configuration :jsonb
#
# Indexes
#
#  index_pipelines_on_ancestry                   (ancestry)
#  index_pipelines_on_project_id                 (project_id)
#  index_pipelines_on_source_type_and_source_id  (source_type,source_id)
#  index_pipelines_on_space_id                   (space_id)
#  index_pipelines_on_template_id                (template_id)
#  index_pipelines_on_user_id                    (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (project_id => projects.id)
#  fk_rails_...  (space_id => spaces.id)
#  fk_rails_...  (template_id => templates.id)
#  fk_rails_...  (user_id => users.id)
#

FactoryGirl.define do
  factory :pipeline do
    name {"#{Faker::App.author}/#{Faker::App.name}"}
    space
    association :source, factory: :repo
    template
    user
    project {create :project, user: user, space: space}
  end
end
