# == Schema Information
#
# Table name: spaces
#
#  id         :integer          not null, primary key
#  name       :string
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_spaces_on_deleted_at  (deleted_at)
#

FactoryGirl.define do
  factory :space do
    name {Faker::Cat.name}
    owner {FactoryGirl.create :user}

    after :build do |space|
      space.users << space.owner
    end
  end
end
