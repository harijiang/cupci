# == Schema Information
#
# Table name: auths
#
#  id           :integer          not null, primary key
#  provider     :string
#  uid          :string
#  user_id      :integer
#  access_token :string
#  expires_at   :datetime
#  info         :jsonb
#  extra        :jsonb
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  deleted_at   :datetime
#
# Indexes
#
#  index_auths_on_deleted_at        (deleted_at)
#  index_auths_on_provider_and_uid  (provider,uid)
#  index_auths_on_user_id           (user_id)
#

FactoryGirl.define do
  factory :auth do
    provider "GithubAuth"
    uid {Faker::Number.number 6}
    access_token {Faker::Crypto.md5}
  end
end
