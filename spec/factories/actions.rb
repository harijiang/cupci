# == Schema Information
#
# Table name: actions
#
#  id               :integer          not null, primary key
#  pipeline_id      :integer
#  sequence         :integer
#  status           :integer          default("initialed")
#  current_pipeline :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  current_stage    :string
#  space_id         :integer
#  started_at       :datetime
#  finished_at      :datetime
#  triggerable_id   :integer
#  triggerable_type :string
#  configuration    :jsonb
#
# Indexes
#
#  index_actions_on_pipeline_id                          (pipeline_id)
#  index_actions_on_pipeline_id_and_sequence             (pipeline_id,sequence) UNIQUE
#  index_actions_on_sequence                             (sequence)
#  index_actions_on_space_id                             (space_id)
#  index_actions_on_status                               (status)
#  index_actions_on_triggerable_id_and_triggerable_type  (triggerable_id,triggerable_type)
#
# Foreign Keys
#
#  fk_rails_...  (pipeline_id => pipelines.id)
#  fk_rails_...  (space_id => spaces.id)
#

FactoryGirl.define do
  factory :action do
    pipeline
    space {pipeline.space}
    association :triggerable, factory: :user
  end
end
