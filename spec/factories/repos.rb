# == Schema Information
#
# Table name: repos
#
#  id         :integer          not null, primary key
#  reponame   :string
#  auth_id    :integer
#  space_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_repos_on_auth_id   (auth_id)
#  index_repos_on_space_id  (space_id)
#
# Foreign Keys
#
#  fk_rails_...  (auth_id => auths.id)
#  fk_rails_...  (space_id => spaces.id)
#

FactoryGirl.define do
  factory :repo do
    reponame {"#{Faker::Cat.registry}/#{Faker::Cat.name}"}
    auth
    space
  end
end
