# == Schema Information
#
# Table name: templates
#
#  id              :integer          not null, primary key
#  name            :string
#  content         :jsonb
#  sequence        :integer
#  template_status :integer          default("privates")
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  space_id        :integer
#
# Indexes
#
#  index_templates_on_sequence         (sequence)
#  index_templates_on_space_id         (space_id)
#  index_templates_on_template_status  (template_status)
#
# Foreign Keys
#
#  fk_rails_...  (space_id => spaces.id)
#

FactoryGirl.define do
  factory :template do
    sequence(:name) {|i| "test #{i}"}
    sequence(:content) {|i| "fake content #{i}"}
    sequence(:sequence) {|i| i}
  end
end
