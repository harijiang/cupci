# scripts input
# output div for ace editor
# textarea for form submit
class ScriptsInput < SimpleForm::Inputs::Base

  class DivTag < ActionView::Helpers::Tags::Base # :nodoc:
    def render
      options = @options.stringify_keys
      add_default_name_and_id(options)

      content_tag("div", options.delete("value") {value_before_type_cast(object)}, options)
    end
  end

  def input(wrapper_options = nil)
    merged_input_options = merge_wrapper_options(input_html_options, wrapper_options)
    html = DivTag.new(@builder.object_name, attribute_name, @builder, merged_input_options.merge(
      style: 'position: relative;width: 100%;height: 200px;',
      "data-editor": true
    )).render
    html << @builder.text_area(attribute_name, merged_input_options.merge(label: false, id: nil))
  end
end