class PipelineWorker
  include Sidekiq::Worker
  sidekiq_options :retry => false, :backtrace => true


  def perform(action_id)
    action = Action.find action_id

    Executor::Manager.run(action) do |executor|
      until action.completed?
        executor.run_next
      end
    end
  end
end
