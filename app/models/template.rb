# == Schema Information
#
# Table name: templates
#
#  id              :integer          not null, primary key
#  name            :string
#  content         :jsonb
#  sequence        :integer
#  template_status :integer          default("privates")
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  space_id        :integer
#
# Indexes
#
#  index_templates_on_sequence         (sequence)
#  index_templates_on_space_id         (space_id)
#  index_templates_on_template_status  (template_status)
#
# Foreign Keys
#
#  fk_rails_...  (space_id => spaces.id)
#

class Template < ApplicationRecord
  has_many :pipelines
  belongs_to :space, required: false
  validates :name, :content, presence: true

  enum template_status: {
    privates: 0,
    published: 1, # can be shared in same space
  }

  scope :default_templates, -> {where("space_id is null and template_status=1").order("sequence")}
  scope :templates, ->(space_id) {where("(space_id is null or space_id=?) and template_status=1", space_id).order("space_id, sequence")}

  # tempate parser
  delegate :stages, to: :parse

  def parse
    @template_parser ||= TemplateParser.new content
  end

  class << self
    # load data
    # scope == :all or Rails env
    def load_templates scope: Rails.env
      logger.debug "======= load templates (scope: #{scope}) ======="
      Dir.glob(File.join Rails.root, 'app', 'lib', 'templates', '**', '*.yml').each do |name|
        content = File.read(name, mode: 'rb')
        parser = TemplateParser.new content
        if scope == :all || parser.scope == scope
          logger.debug "load template #{name}"
          Template.create! name: parser.name, content: content
        else
          logger.debug "skip template #{name}, template scope: #{parser.scope}"
        end
      end
    end
  end
end
