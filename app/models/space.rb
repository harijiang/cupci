# == Schema Information
#
# Table name: spaces
#
#  id         :integer          not null, primary key
#  name       :string
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_spaces_on_deleted_at  (deleted_at)
#

class Space < ApplicationRecord
  # many to many with users
  has_many :spaces_users
  has_many :users, through: :spaces_users

  # associations
  has_many :pipelines
  has_many :repos
  has_many :actions
  has_many :projects

  attr_accessor :owner

  after_commit :set_space_admin, on: :create

  acts_as_paranoid

  validates :name, presence: true
  validate :validate_admin_spaces_name, on: :create

  def admins
    spaces_users.where(role: :admin).includes(:user).map(&:user)
  end

  private
  def set_space_admin
    spaces_users.where(user: owner).first.admin!
  end

  def validate_admin_spaces_name
    if owner.blank?
      errors[:owner] << 'Space must have an owner'
      return
    end
    if owner.spaces.where(name: name).exists?
      errors[:name] << 'You already have a workspace with this name'
    end
  end
end
