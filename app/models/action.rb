# == Schema Information
#
# Table name: actions
#
#  id               :integer          not null, primary key
#  pipeline_id      :integer
#  sequence         :integer
#  status           :integer          default("initialed")
#  current_pipeline :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  current_stage    :string
#  space_id         :integer
#  started_at       :datetime
#  finished_at      :datetime
#  triggerable_id   :integer
#  triggerable_type :string
#  configuration    :jsonb
#
# Indexes
#
#  index_actions_on_pipeline_id                          (pipeline_id)
#  index_actions_on_pipeline_id_and_sequence             (pipeline_id,sequence) UNIQUE
#  index_actions_on_sequence                             (sequence)
#  index_actions_on_space_id                             (space_id)
#  index_actions_on_status                               (status)
#  index_actions_on_triggerable_id_and_triggerable_type  (triggerable_id,triggerable_type)
#
# Foreign Keys
#
#  fk_rails_...  (pipeline_id => pipelines.id)
#  fk_rails_...  (space_id => spaces.id)
#

# need current_pipeline current_step to location next to execute
class Action < ApplicationRecord
  belongs_to :space
  belongs_to :pipeline
  belongs_to :triggerable, polymorphic: true

  before_save do
    self.sequence = pipeline.actions.count + 1
  end

  enum status: {
    initialed: 0,
    processing: 1,
    successed: 97,
    canceled: 98,
    failed: 99,
  }

  validates :space_id, :pipeline_id, :triggerable, presence: true

  cattr_reader(:completed_statuses) {[:successed, :canceled, :failed].freeze}

  scope :running, -> {where.not(status: completed_statuses)}
  scope :completed, -> {where(status: completed_statuses)}

  # helper methods

  def name
    "#{pipeline.source.reponame}-#{configuration&.dig('git_commit')}-#{sequence}"
  end

  def ran_seconds
    finished_at - started_at
  end

  # errors
  class StatusError < StandardError
  end

  # action workflow

  def completed?
    Action.completed_statuses.include? status.to_sym
  end

  def next_stage
    stages = pipeline.template.stages
    if current_stage.blank?
      stages.first
    else
      stages[stages.find_index {|s| s.name==current_stage} + 1]
    end
  end

  def initialize_current_action
    update_attributes! current_pipeline: pipeline, status: :processing, started_at: Time.now
  end

  # Start background build job
  def start_build!
    # reset attributes
    update_columns current_stage: nil, finished_at: nil, status: :initialed
    PipelineWorker.perform_async(id)
  end
end
