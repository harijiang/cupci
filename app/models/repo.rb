# == Schema Information
#
# Table name: repos
#
#  id         :integer          not null, primary key
#  reponame   :string
#  auth_id    :integer
#  space_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_repos_on_auth_id   (auth_id)
#  index_repos_on_space_id  (space_id)
#
# Foreign Keys
#
#  fk_rails_...  (auth_id => auths.id)
#  fk_rails_...  (space_id => spaces.id)
#

class Repo < ApplicationRecord
  belongs_to :auth
  belongs_to :space
  has_one :pipeline, as: :source

  validates :space_id, :auth_id, :reponame, presence: true
end
