# == Schema Information
#
# Table name: auths
#
#  id           :integer          not null, primary key
#  provider     :string
#  uid          :string
#  user_id      :integer
#  access_token :string
#  expires_at   :datetime
#  info         :jsonb
#  extra        :jsonb
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  deleted_at   :datetime
#
# Indexes
#
#  index_auths_on_deleted_at        (deleted_at)
#  index_auths_on_provider_and_uid  (provider,uid)
#  index_auths_on_user_id           (user_id)
#

class GithubAuth < Auth
  class << self
    def from_omniauth(auth_info)
      attributes={
        access_token: auth_info.credentials.token,
        info: auth_info.info,
        extra: auth_info.extra
      }
      if (auth = where(uid: auth_info.uid).first_or_initialize(attributes)).persisted?
        auth.update_columns attributes
      else
        auth.save!
      end
      auth
    end
  end
end
