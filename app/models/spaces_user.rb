# == Schema Information
#
# Table name: spaces_users
#
#  id         :integer          not null, primary key
#  space_id   :integer
#  user_id    :integer
#  role       :integer          default("member")
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_spaces_users_on_deleted_at  (deleted_at)
#  index_spaces_users_on_space_id    (space_id)
#  index_spaces_users_on_user_id     (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (space_id => spaces.id)
#  fk_rails_...  (user_id => users.id)
#

class SpacesUser < ApplicationRecord
  belongs_to :space
  belongs_to :user

  acts_as_paranoid

  enum role: {member: 0, admin: 1}
end
