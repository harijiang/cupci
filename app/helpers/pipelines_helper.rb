module PipelinesHelper
  def render_pipeline_status(pipeline)
    if (running_count = pipeline.actions.running.count) > 0
      "<span class=\"text-success\">Running (#{running_count})</span>"
    else
      "<span>Not Running</span>"
    end.html_safe
  end
end
