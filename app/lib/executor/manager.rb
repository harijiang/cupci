module Executor
  class Manager
    include ActiveSupport::Callbacks
    define_callbacks :run

    include Log

    attr_reader :action

    def initialize(action, options)
      @action = action
      @options = options
    end

    # run action
    def run_next
      case @action.status.to_sym
        when :initialed
          @action.initialize_current_action
          run_stage
        when :processing
          run_stage
        else
          raise Action::StatusError, "unknown current status '#{@action.status}'"
      end
    end

    def run_stage
      # return if set to canceled
      return if @action.completed?

      # fetch next stage
      stage = @action.next_stage
      @action.update_attributes! current_stage: stage.name

      # initial executor
      executor = new_executor(stage.type)

      begin
        executor.run_stage stage
      ensure
        # set current status
        @action.failed!
      end

      if @action.next_stage.blank?
        @action.successed!
      end
    end

    private

    def new_executor(type)
      Executor::Manager.new_executor(type, **@options)
    end

    class << self
      def run(action, **options)
        executor_manager = new(action, options)
        executor_manager.run_callbacks :run do
          yield executor_manager
        end
      end

      # Create {Type}Executor instant
      def new_executor(type, *args, **options)
        executor_klass = "Executor::#{type.camelize}Executor".constantize
        executor_klass.new *args, **options
      end
    end
  end
end