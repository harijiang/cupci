require 'fileutils'

module Executor::Manager::Log
  extend ActiveSupport::Concern

  included do
    set_callback :run, :after do |object|
      log_file.close
    end
  end

  def stdout
    @options[:stdout] || log_file
  end

  def stderr
    @options[:stderr] || log_file
  end

  def log_file
    @log_file ||= begin
      dirname = File.dirname(log_path)
      unless File.directory?(dirname)
        FileUtils.mkdir_p(dirname)
      end
      File.open(log_path, 'a+')
    end
  end

  def log_path
    @log_path ||= File.join(Settings.build_actions.log_dir, "pipelines/#{action.pipeline.id}", "actions/#{action.id}", 'build.log')
  end
end