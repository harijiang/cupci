module Executor
  # Base for all executors
  # Executors must inherited Base class and named as SomeExecutor, otherwise may cause error
  class Base
    include ActiveSupport::Configurable

    def initialize(**options)
      @options = options
    end

    # Executor a pipeline stage
    def run_stage(stage)
    end
  end
end