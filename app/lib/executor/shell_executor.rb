module Executor
  class ShellExecutor < Base
    def initialize(stderr: nil, stdout: nil)
      # IO.popen3
    end

    def run_line line
      puts line
    end

    def run_stage stage
      stage.steps.each do |step|
        run_line step
      end
    end
  end
end
