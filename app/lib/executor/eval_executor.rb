require 'stringio'

module Executor
  class EvalExecutor < Base
    def initialize(locals: {}, stderr: nil, stdout: nil)
      if Rails.env.production?
        raise SecurityError, 'EvalExecutor not allow initialize in production env'
      end

      @locals = locals.merge(stdout: stdout, stderr: stderr)

      @locals.each_pair do |k, v|
        context.local_variable_set k, v
      end
    end

    def run_line line
      context.eval line
    end

    def run_stage stage
      stage.steps.each do |step|
        run_line step
      end
    end

    protected
    def context
      @_context_binding ||= Object.new.send :binding
    end
  end
end
