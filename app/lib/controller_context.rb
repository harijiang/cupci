# included by ApplicationController or some Helper
module ControllerContext
  def current_space
    space_id = params[:space_id]
    current_user.spaces.find(space_id)
  end
end