# Decorator for model json or jsonb field
# https://github.com/plataformatec/simple_form/wiki/Nested-inputs-for-key-value-hash-attributes
module Decorator
  class JsonField

    MODEL_NAME = ActiveModel::Name.new(self.class, nil, 'configuration')

    def model_name
      MODEL_NAME
    end

    def initialize(hash)
      hash ||= {}
      @object = hash.symbolize_keys
    end

    # Delegates to the wrapped object
    def method_missing(method, *args, &block)
      if @object.key? method
        @object[method]
      elsif @object.respond_to? method
        @object.send(method, *args, &block)
      end
    end

    def has_attribute? attr
      @object.key? attr
    end
  end
end