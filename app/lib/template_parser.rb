class TemplateParser
  attr_reader :content

  def initialize content
    @content = content
  end

  def parsed_content
    @parsed_content ||= DeepStruct.new YAML.load @content
  end

  delegate_missing_to :parsed_content
end