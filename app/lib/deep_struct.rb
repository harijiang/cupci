# modify from http://andreapavoni.com/blog/2013/4/create-recursive-openstruct-from-a-ruby-hash/#.WZ231pMjE0o

require 'ostruct'

class DeepStruct < OpenStruct
  def initialize(hash=nil)
    @table = {}
    @hash_table = {}

    if hash
      hash.each do |k, v|
        @table[k.to_sym] = convert_entry(v)
        @hash_table[k.to_sym] = v

        new_ostruct_member(k)
      end
    end
  end

  def to_h
    @hash_table
  end

  private
  def convert_entry(value)
    if value.is_a?(Hash)
      self.class.new(value)
    elsif value.is_a?(Array)
      value.map {|i|
        convert_entry(i)
      }
    else
      value
    end
  end
end