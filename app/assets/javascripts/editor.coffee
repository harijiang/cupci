Editor = {}
Editor.init_editor = ->
  $('div[data-editor=true]').each ->
    editor = ace.edit(@.id)
    textarea = $(@).next()
    unless textarea.is('textarea')
      return
    textarea.hide()
    editor.getSession().setValue(textarea.val())
    editor.getSession().on 'change', ->
      textarea.val(editor.getSession().getValue())

App.Editor = Editor