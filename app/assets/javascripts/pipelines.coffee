# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
class Pipelines
  init: =>
    @init_select_projects()
  init_select_projects: =>
    $('#select-projects').selectize({
      create: true,
      placeholder: 'Chose or input a project name  ',
    })

App.Controllers.Pipelines = Pipelines