window.App ||= {Controllers: {}}

# registry events from Controller.events
# Example:
# class Controller
#   alert: ->
#     alert "your are hacked!"
#   events:
#     "click a": @alert

App.init = ->
  controllerName = $('body').data('controller')
  controllerKlass = window.App.Controllers?[controllerName]
  unless controllerKlass
    return
  controller = new controllerKlass
  # call init
  controller.init()
  # register events
  for key of controller.events
    sep = key.indexOf(' ')
    event = key.substr(0, sep);
    selector = key.substr(sep + 1);
    handler = controller.events[key]
    $(selector).on event, handler

App.initEvents = ->
  App.Editor.init_editor()

$(document).ready ->
  App.init()
  App.initEvents()