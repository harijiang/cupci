class SpacesController < ApplicationController

  def index
    @spaces = current_user.spaces
  end

  def create
    @space = current_user.spaces.create space_params.merge(owner: current_user)
  end

  private
  def space_params
    params.require(:space).permit(:name)
  end
end
