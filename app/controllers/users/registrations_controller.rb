class Users::RegistrationsController < Devise::RegistrationsController
  before_action :render_404, except: [:new, :create]

  skip_before_action :check_user_space
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  layout 'application'

  # GET /resource/sign_up
  # def new
  #   super
  # end

  # POST /resource
  def create
    ApplicationRecord.transaction do
      super do |user|
        if (data = session["devise.omniauth_data"]&.with_indifferent_access).present? && user.persisted?
          auth = Auth.with_provider(data[:provider]).find_by(uid: data[:uid])
          auth.update user: user
        else
          logger.error "user persisted #{user.persisted?}: #{user.attributes}\nomniauth_data: #{session["devise.omniauth_data"]}"
          user.destroy if user.persisted?
        end
      end
    end
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the users wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
