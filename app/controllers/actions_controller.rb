class ActionsController < ApplicationController
  before_action :check_user_current_space

  def index
    @q = current_space.actions.ransack(params[:q])
    @actions = @q.result.order(id: :desc).includes(:pipeline).page(params[:page])
  end

  def show
    set_action
  end

  def rebuild
    set_action
    @action.start_build!
  end

  private
  def set_action
    @action = current_space.actions.find(params[:id])
  end
end
