class TemplatesController < ApplicationController
  before_action :check_user_current_space

  def index
    @templates = Template.templates(current_space)
  end
end
