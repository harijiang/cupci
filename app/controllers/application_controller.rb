class ApplicationController < ActionController::Base
  include ControllerContext
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  layout 'dashboard'

  def url_options
    {space_id: params[:space_id] || cookies[:space_id] || 'default'}.merge(super)
  end

  def after_sign_in_path_for(resource)
    cookies[:space_id] && dashboard_url(space_id: cookies[:space_id]) || request.env['omniauth.origin'] || stored_location_for(resource)
  end

  def check_user_current_space
    current_space
  rescue ActiveRecord::RecordNotFound
    redirect_to({controller: 'spaces', action: 'index'}, alert: 'Create a workspace')
  end

  def render_404
    raise ActionController::RoutingError.new('Not Found')
  end
end
