class Pipelines::ActionsController < ApplicationController
  def new
    set_pipeline
    build_action
  end

  def create
    set_pipeline
    build_action
    if @action.save
      @action.start_build!
    end
  end

  private

  def set_pipeline
    @pipeline = current_space.pipelines.find(params[:pipeline_id])
  end

  def build_action
    @action ||= @pipeline.actions.new
    @action.attributes = action_params.merge(space: current_space, triggerable: current_user, configuration: action_configuration_params)
  end

  def action_params
    {}
  end

  def action_configuration_params
    if params.dig(:action_attributes, :configuration)
      params.require(:action_attributes).require(:configuration).permit!
    else
      nil
    end
  end
end
