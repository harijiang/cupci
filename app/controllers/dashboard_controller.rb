class DashboardController < ApplicationController
  before_action :check_user_current_space

  def index
    # remember space_id
    cookies[:space_id] ||= params[:space_id]
    redirect_to space_actions_path
  end
end
