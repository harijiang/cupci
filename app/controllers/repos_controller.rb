class ReposController < ApplicationController

  def github_repos
    query = params[:query]
    return render(json: {repos: []}) if query.blank?

    auth = current_user.auths.with_provider('github').first!
    client = Octokit::Client.new(access_token: auth.access_token)

    # try find login name
    if query.include? '/'
      login, query = query.split '/'
    elsif query.size >= 2
      login = client.orgs.select {|o| o.login.include? query}.first&.login
      query = nil if login.present?
    else
      login = auth.extra[:raw_info][:login]
    end

    repos =loop_all_repos_pages {|page:, per_page:|
      client.repos(login, sort: 'pushed', direction: 'desc', per_page: per_page, page: page)
    }
    # repos.select! {|repo| repo[:name].include? query} if query.present?
    render json: {repos: repos.map(&:to_h)}
  end

  private
  # fetch all pages
  def loop_all_repos_pages
    per_page = 50
    page = 1
    repos = []
    begin
      result = yield per_page: per_page, page: page
      repos += result
      page += 1
    end while result.size >= per_page
    repos
  end
end
