class PipelinesController < ApplicationController
  before_action :check_user_current_space

  def create
    set_template
    build_pipeline

    if save_pipeline
      redirect_to action: :index
    else
      render :new
    end
  end

  def new
    set_template
    @pipeline = Pipeline.new template: @template
  end

  def show
    set_pipeline
  end

  def edit
    set_pipeline
  end

  def configuration
    set_pipeline
  end

  def destroy
    set_pipeline
    @pipeline.destroy

    respond_to do |format|
      format.html do
        redirect_to action: :index
      end

      format.js
    end
  end

  def update
    set_template
    set_pipeline
    build_pipeline
    if save_pipeline
      redirect_to action: :show
    else
      render :edit
    end
  end

  def index
    @q = current_space.pipelines.ransack(params[:q])
    @pipelines = @q.result.order(updated_at: :desc).includes(:project).page(params[:page])
  end

  private
  # save pipeline and dependencies
  def save_pipeline
    valid = true
    valid = @pipeline.source.valid? && valid
    valid = @pipeline.project.valid? && valid
    valid = @pipeline.valid? && valid
    if valid
      Pipeline.transaction do
        @pipeline.source.save!
        @pipeline.project.save!
        @pipeline.save!
      end
      true
    else
      false
    end
  end

  def build_pipeline
    @pipeline ||= current_space.pipelines.new
    source = load_pipeline_source
    project = find_or_create_project
    @pipeline.attributes = pipeline_params.merge(
      template: @template, source: source, user: current_user, project: project,
      configuration: pipeline_configuration_params)
  end

  def load_pipeline_source
    auth = current_user.auths.find repo_params[:auth_id]
    attributes = {auth: auth, reponame: repo_params[:reponame]}

    # build or update repo as source for each pipeline
    source = @pipeline&.source || current_space.repos.new
    source.attributes = attributes
    source
  end

  def find_or_create_project
    name = params.dig(:pipeline, :project, :name)
    current_space.projects.where(user: current_user).find_or_create_by name: name
  end

  def set_template
    @template = Template.templates(current_space).find(params.dig(:pipeline, :template_id))
  end

  def set_pipeline
    @pipeline = current_space.pipelines.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def pipeline_params
    params.require(:pipeline).permit(:name)
  end

  def pipeline_configuration_params
    if params.dig(:pipeline, :configuration).present?
      params.require(:pipeline).require(:configuration).permit!
    else
      nil
    end
  end

  def repo_params
    params.require(:pipeline).require(:source).permit(:reponame, :auth_id)
  end
end
