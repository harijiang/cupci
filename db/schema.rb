# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170818112806) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "actions", force: :cascade do |t|
    t.bigint "pipeline_id"
    t.integer "sequence"
    t.integer "status", default: 0
    t.integer "current_pipeline"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "current_stage"
    t.bigint "space_id"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.integer "triggerable_id"
    t.string "triggerable_type"
    t.jsonb "configuration"
    t.index ["pipeline_id", "sequence"], name: "index_actions_on_pipeline_id_and_sequence", unique: true
    t.index ["pipeline_id"], name: "index_actions_on_pipeline_id"
    t.index ["sequence"], name: "index_actions_on_sequence"
    t.index ["space_id"], name: "index_actions_on_space_id"
    t.index ["status"], name: "index_actions_on_status"
    t.index ["triggerable_id", "triggerable_type"], name: "index_actions_on_triggerable_id_and_triggerable_type"
  end

  create_table "auths", force: :cascade do |t|
    t.string "provider"
    t.string "uid"
    t.bigint "user_id"
    t.string "access_token"
    t.datetime "expires_at"
    t.jsonb "info"
    t.jsonb "extra"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_auths_on_deleted_at"
    t.index ["provider", "uid"], name: "index_auths_on_provider_and_uid"
    t.index ["user_id"], name: "index_auths_on_user_id"
  end

  create_table "pipelines", force: :cascade do |t|
    t.string "name"
    t.bigint "space_id"
    t.string "source_type"
    t.bigint "source_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ancestry"
    t.bigint "template_id"
    t.bigint "user_id"
    t.bigint "project_id"
    t.jsonb "configuration"
    t.index ["ancestry"], name: "index_pipelines_on_ancestry"
    t.index ["project_id"], name: "index_pipelines_on_project_id"
    t.index ["source_type", "source_id"], name: "index_pipelines_on_source_type_and_source_id"
    t.index ["space_id"], name: "index_pipelines_on_space_id"
    t.index ["template_id"], name: "index_pipelines_on_template_id"
    t.index ["user_id"], name: "index_pipelines_on_user_id"
  end

  create_table "projects", force: :cascade do |t|
    t.string "name"
    t.bigint "space_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["space_id"], name: "index_projects_on_space_id"
    t.index ["user_id"], name: "index_projects_on_user_id"
  end

  create_table "repos", force: :cascade do |t|
    t.string "reponame"
    t.bigint "auth_id"
    t.bigint "space_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["auth_id"], name: "index_repos_on_auth_id"
    t.index ["space_id"], name: "index_repos_on_space_id"
  end

  create_table "spaces", force: :cascade do |t|
    t.string "name"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["deleted_at"], name: "index_spaces_on_deleted_at"
  end

  create_table "spaces_users", force: :cascade do |t|
    t.bigint "space_id"
    t.bigint "user_id"
    t.integer "role", default: 0
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["deleted_at"], name: "index_spaces_users_on_deleted_at"
    t.index ["space_id"], name: "index_spaces_users_on_space_id"
    t.index ["user_id"], name: "index_spaces_users_on_user_id"
  end

  create_table "templates", force: :cascade do |t|
    t.string "name"
    t.jsonb "content"
    t.integer "sequence"
    t.integer "template_status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "space_id"
    t.index ["sequence"], name: "index_templates_on_sequence"
    t.index ["space_id"], name: "index_templates_on_space_id"
    t.index ["template_status"], name: "index_templates_on_template_status"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "provider"
    t.string "uid"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_users_on_deleted_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  add_foreign_key "actions", "pipelines"
  add_foreign_key "actions", "spaces"
  add_foreign_key "pipelines", "projects"
  add_foreign_key "pipelines", "spaces"
  add_foreign_key "pipelines", "templates"
  add_foreign_key "pipelines", "users"
  add_foreign_key "projects", "spaces"
  add_foreign_key "projects", "users"
  add_foreign_key "repos", "auths"
  add_foreign_key "repos", "spaces"
  add_foreign_key "spaces_users", "spaces"
  add_foreign_key "spaces_users", "users"
  add_foreign_key "templates", "spaces"
end
