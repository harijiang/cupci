class AddAncestryToPipeline < ActiveRecord::Migration[5.1]
  def change
    add_column :pipelines, :ancestry, :string
    add_index :pipelines, :ancestry
  end
end
