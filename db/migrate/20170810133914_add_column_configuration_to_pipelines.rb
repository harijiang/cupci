class AddColumnConfigurationToPipelines < ActiveRecord::Migration[5.1]
  def change
    add_column :pipelines, :configuration, :jsonb
  end
end
