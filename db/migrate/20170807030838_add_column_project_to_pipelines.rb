class AddColumnProjectToPipelines < ActiveRecord::Migration[5.1]
  def change
    add_reference :pipelines, :project, foreign_key: true
  end
end
