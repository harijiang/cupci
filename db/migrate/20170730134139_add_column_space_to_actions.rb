class AddColumnSpaceToActions < ActiveRecord::Migration[5.1]
  def change
    add_reference :actions, :space, foreign_key: true
  end
end
