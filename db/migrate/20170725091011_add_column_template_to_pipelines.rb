class AddColumnTemplateToPipelines < ActiveRecord::Migration[5.1]
  def change
    add_reference :pipelines, :template, foreign_key: true
  end
end
