class AddColumnSpaceToTemplates < ActiveRecord::Migration[5.1]
  def change
    add_reference :templates, :space, foreign_key: true
  end
end
