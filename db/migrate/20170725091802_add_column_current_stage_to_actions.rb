class AddColumnCurrentStageToActions < ActiveRecord::Migration[5.1]
  def change
    add_column :actions, :current_stage, :string
  end
end
