class AddColumnStartedAtAndFinishedAtToActions < ActiveRecord::Migration[5.1]
  def change
    add_column :actions, :started_at, :datetime
    add_column :actions, :finished_at, :datetime
  end
end
