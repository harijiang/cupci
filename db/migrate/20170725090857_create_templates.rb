class CreateTemplates < ActiveRecord::Migration[5.1]
  def change
    create_table :templates do |t|
      t.string :name
      t.jsonb :content
      t.integer :sequence, index: true
      t.integer :template_status, index: true, default: 0

      t.timestamps
    end
  end
end
