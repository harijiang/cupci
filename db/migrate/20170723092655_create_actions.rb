class CreateActions < ActiveRecord::Migration[5.1]
  def change
    create_table :actions do |t|
      t.references :pipeline, foreign_key: true
      t.integer :sequence, index: true
      t.integer :status, index: true, default: 0
      t.integer :current_pipeline

      t.timestamps
    end
  end
end
