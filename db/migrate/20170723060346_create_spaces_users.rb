class CreateSpacesUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :spaces_users do |t|
      t.references :space, foreign_key: true
      t.references :user, foreign_key: true
      t.integer :role, default: 0
      t.datetime :deleted_at, index: true

      t.timestamps
    end
  end
end
