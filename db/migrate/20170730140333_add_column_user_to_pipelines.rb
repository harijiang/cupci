class AddColumnUserToPipelines < ActiveRecord::Migration[5.1]
  def change
    add_reference :pipelines, :user, foreign_key: true
  end
end
