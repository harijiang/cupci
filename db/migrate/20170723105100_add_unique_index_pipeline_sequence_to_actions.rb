class AddUniqueIndexPipelineSequenceToActions < ActiveRecord::Migration[5.1]
  def change
    add_index :actions, [:pipeline_id, :sequence], unique: true
  end
end
