class CreateAuths < ActiveRecord::Migration[5.1]
  def change
    create_table :auths do |t|
      t.string :provider
      t.string :uid
      t.references :user, index: true
      t.string :access_token
      t.datetime :expires_at
      t.jsonb :info
      t.jsonb :extra

      t.timestamps
    end

    add_index :auths, [:provider, :uid]
  end
end
