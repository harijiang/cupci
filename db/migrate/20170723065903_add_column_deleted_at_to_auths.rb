class AddColumnDeletedAtToAuths < ActiveRecord::Migration[5.1]
  def change
    add_column :auths, :deleted_at, :datetime
    add_index :auths, :deleted_at
  end
end
