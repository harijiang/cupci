class CreateRepos < ActiveRecord::Migration[5.1]
  def change
    create_table :repos do |t|
      t.string :reponame
      t.references :auth, foreign_key: true
      t.references :space, foreign_key: true

      t.timestamps
    end
  end
end
