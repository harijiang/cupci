class AddColumnTriggerableIdAndTriggerableTypeToActions < ActiveRecord::Migration[5.1]
  def change
    add_column :actions, :triggerable_id, :integer
    add_column :actions, :triggerable_type, :string
    add_index :actions, [:triggerable_id, :triggerable_type]
  end
end
