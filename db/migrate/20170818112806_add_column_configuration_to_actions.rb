class AddColumnConfigurationToActions < ActiveRecord::Migration[5.1]
  def change
    add_column :actions, :configuration, :jsonb
  end
end
